using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BuildBoard : MonoBehaviour
{
	public Sprite myMap;
	public MapElement[] myMapElements;
	public GameObject emptyGameObject;
	public AudioManager manager;
	public Transform tileParent;
	public Transform spawnParent;
	public GameObject Player;
	public Text Happiness;
	public Text VictimCounter;
	public Text endOfGame;
	GameObject sacrificalAltar;
	Color[] myColors;
	public int height, width;
	Vector2 UpperLeftCorner;
	List<Transform> playerSpawnPoints = new List<Transform>();
	List<Transform> vicitmSpawnPoints = new List<Transform>();
	// Start is called before the first frame update
	void Start()
    {
		GenerateMap();
		GenerateSpawnLists();
		if (playerSpawnPoints.Count > 0)
		{
			Transform[] playerPositions = playerSpawnPoints.ToArray();
			int playerIndex = Random.Range(0, playerPositions.Length);
			Player.transform.position = playerPositions[playerIndex].position;
		}
    }

	private void GenerateSpawnLists()
	{
		if (sacrificalAltar != null)
		{
			DietyScript dietyScript = sacrificalAltar.GetComponent<DietyScript>();
			if (dietyScript !=null)
			{				
				dietyScript.Spawnpoints = vicitmSpawnPoints.ToArray();
				dietyScript.myHappiness = Happiness;
				dietyScript.myVictCount = VictimCounter;
				dietyScript.endOfGame = endOfGame;
				dietyScript.audioManager = manager;
				dietyScript.playerMove = Player.GetComponent<PlayerMovement>();
			}
		}
	}

	private void GenerateMap()
	{
		UpperLeftCorner = new Vector2
		{
			x = -width / 2,
			y = -height / 2
		};

		myColors = myMap.texture.GetPixels();
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				GenerateTile(x, y, myColors[y * width + x]);
			}
		}

	}
	private void GenerateTile(int x, int y, Color color)
	{
		for (int i = 0; i < myMapElements.Length; i++)
		{
			if (myMapElements[i].myColor ==color)
			{				
				GameObject block = Instantiate(myMapElements[i].myPrefab,new Vector3(x + UpperLeftCorner.x, y + UpperLeftCorner.y, 0), Quaternion.identity);
				block.transform.SetParent(tileParent);
				if (i==3 && sacrificalAltar==null)
				{
					sacrificalAltar = block;
				}

				if (myMapElements[i].SpawnVictim)
				{
					GameObject victimSpawn = Instantiate(emptyGameObject, new Vector3(x + UpperLeftCorner.x, y + UpperLeftCorner.y, 0), Quaternion.identity);
					victimSpawn.transform.SetParent(spawnParent);
					vicitmSpawnPoints.Add(victimSpawn.transform);
				}

				if (myMapElements[i].SpawnPlayer)
				{
					GameObject playerSpawn = Instantiate(emptyGameObject, new Vector3(x + UpperLeftCorner.x, y + UpperLeftCorner.y, 0), Quaternion.identity);
					playerSpawn.transform.SetParent(spawnParent);
					playerSpawnPoints.Add(playerSpawn.transform);
				}
				return;
			}
		}
	}

	// Update is called once per frame
	void Update()
    {
        
    }
}
