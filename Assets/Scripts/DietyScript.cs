using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Sprite))]
public class DietyScript : MonoBehaviour
{
	SpriteRenderer mySprite;
	public GameObject SpawnPrefab;
	public GameObject Punishment;
	public GameObject bloodSplat;
	public AudioManager audioManager;
	public PlayerMovement playerMove;
	public Color myColor;
	public Transform[] Spawnpoints;
	int sacrificeIndex;
	int sacrificeColor;
	public float happiness = 100.0f;
	public float happinessDecrease = 2.0f;
	public Text myHappiness;
	private int countOfVictims;
	public Text myVictCount;
	public Text endOfGame;
	float punishmentTimer, feedMeTimer;
	public float punishmentTimeBetween = 2.0f;
	public string[] splatsSounds;
	public string[] hungerSounds;
	public string[] wrongFood;
	public string[] loseSounds;
	private bool gameOver;

	public void AddVictim()
	{
		countOfVictims += 1;
		if (myVictCount != null)
		{
			myVictCount.text = "Victims:"+countOfVictims.ToString();
		}

	}

	// Start is called before the first frame update
	void Start()
	{
		mySprite = GetComponent<SpriteRenderer>();
		SpawnNewSacrifice();
		if (myVictCount != null)
		{
			myVictCount.text = "Victims:" + countOfVictims.ToString();
		}
	}

	private void Update()
	{
		if (!gameOver)
		{
			happiness -= (happinessDecrease * Time.deltaTime);
			if (myHappiness != null)
			{
				myHappiness.text = happiness.ToString();
			}

			if (happiness < 50.0f && punishmentTimer <= 0.0f)
			{
				punishmentTimer = (happiness / 100.0f);
				SpawnProblem();
			}
			punishmentTimer -= Time.deltaTime;

			if (feedMeTimer <= 0.0f)
			{
				feedMeTimer = 4.0f + Random.Range(-1.5f, 1.75f);
			}
			feedMeTimer -= Time.deltaTime;

			if (happiness < 0.0f && !gameOver)
			{
				myHappiness.text = "You have LOST!";
				if (audioManager != null && loseSounds != null)
				{
					audioManager.Play(loseSounds[Random.Range(0, loseSounds.Length)]);
				}
				//Time.timeScale = 0.0f;
				endOfGame.text = "Game Over\n\nPress R to Restart";
				gameOver = true;

			}
		}
		else
		{
			playerMove.speed = 0;
			if (Input.GetKeyDown(KeyCode.R))
			{
				SceneManager.LoadScene(SceneManager.GetActiveScene().name);
			}
		}
	}

	void SpawnProblem()
	{
		float XPos, YPos;
		XPos = Random.Range(-10, 10) / 1.0f;
		YPos = Random.Range(-10, 10) / 1.0f;
		while (XPos == Spawnpoints[sacrificeIndex].position.x && YPos == Spawnpoints[sacrificeIndex].position.y && XPos !=0 && YPos !=0)
		{
			XPos = Random.Range(-10, 10) / 1.0f;
			YPos = Random.Range(-10, 10) / 1.0f;
		}
		Vector3 position = new Vector3(XPos, YPos, 0.0f);
		Instantiate(Punishment, position, Quaternion.identity);
	}

	public void SpawnNewSacrifice()
	{
		//create a color
		sacrificeColor = Random.Range(0, 11);
		myColor = Color.HSVToRGB(sacrificeColor / 10.0f, 1.0f, 1.0f);
		mySprite.color = myColor;
		if (Spawnpoints != null)
		{
			sacrificeIndex = Random.Range(0, Spawnpoints.Length);
			Transform myLocation = Spawnpoints[sacrificeIndex];
			if (SpawnPrefab != null)
			{
				GameObject newPerson = Instantiate(SpawnPrefab, myLocation.position, Quaternion.identity);
				if (newPerson != null)
				{
					SpriteRenderer personSR = newPerson.GetComponent<SpriteRenderer>();
					if (personSR != null)
					{
						personSR.color = myColor;
					}
				}
			}
			for (int i = 0; i < Random.Range(0, 4); i++)
			{
				SpawnNonSacrifice();
			}
		}
	}

	void SpawnNonSacrifice()
	{
		int nonSacrificeColor = Random.Range(0, 11);
		while (nonSacrificeColor == sacrificeColor)
		{
			nonSacrificeColor = Random.Range(0, 11);
		}

		int nonSacrificeIndex = Random.Range(0, Spawnpoints.Length);
		while (nonSacrificeIndex == sacrificeIndex)
		{
			nonSacrificeIndex = Random.Range(0, Spawnpoints.Length);
		}

		Transform myLocation = Spawnpoints[nonSacrificeIndex];
		GameObject newPerson = Instantiate(SpawnPrefab, myLocation.position, Quaternion.identity);
		if (newPerson != null)
		{
			SpriteRenderer personSR = newPerson.GetComponent<SpriteRenderer>();
			if (personSR != null)
			{
				Color spawnColor = Color.HSVToRGB(nonSacrificeColor / 10.0f, 1.0f, 1.0f);
				personSR.color = spawnColor;
			}
		}

	}


	public void ChangeHappiness(float value)
	{
		happiness += value;
	}

	public void SpawnBlood()
	{
		if (audioManager != null && splatsSounds != null)
		{
			audioManager.Play(splatsSounds[Random.Range(0, splatsSounds.Length)]);
		}
		Instantiate(bloodSplat, transform.position, Quaternion.identity);
	}

	public void WrongFood()
	{
		if (audioManager != null && wrongFood != null)
		{
			audioManager.Play(wrongFood[Random.Range(0, wrongFood.Length)]);
		}

	}


}
