using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SacrificeMovement : MonoBehaviour
{
	public Transform target;
	public float speed = 5.0f;
	SpriteRenderer mySR;
    // Start is called before the first frame update
    void Start()
    {
		mySR = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
		if (target != null)
		{
			transform.Translate((target.position - transform.position) * speed * Time.deltaTime);
		}
    }
	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag=="Goal")
		{
			DietyScript myScript = collision.gameObject.GetComponent<DietyScript>();
			if (mySR.color == myScript.myColor)
			{
				myScript.SpawnNewSacrifice();
				myScript.ChangeHappiness(15.00f);
				myScript.SpawnBlood();
				myScript.AddVictim();
				GameObject.Destroy(gameObject);
			}
			else
			{
				myScript.ChangeHappiness(-5.00f);
				myScript.SpawnBlood();
				myScript.WrongFood();
				GameObject.Destroy(gameObject);
			}
		}

		if (collision.tag == "Player")
		{
			target = collision.transform;
		}
	}
}
