using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Punishment : MonoBehaviour
{
	public float Delay = 2.0f;

    // Update is called once per frame
    void Update()
    {
		Delay -= Time.deltaTime;
		if (Delay<0)
		{
			GameObject.Destroy(gameObject);
		}
    }
}
