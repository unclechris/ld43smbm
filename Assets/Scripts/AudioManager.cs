using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
	public Sound[] mySounds;
	private void Awake()
	{
		foreach (var sound in mySounds)
		{
			sound.source = gameObject.AddComponent<AudioSource>();
			sound.source.clip = sound.clip;
			sound.source.pitch = sound.pitch;
			sound.source.loop = sound.loop;
		}
	}
	// Start is called before the first frame update
	void Start()
    {
		Play("Theme");
	}

	public void Play(string v)
	{
		Sound s = Array.Find(mySounds, sound => sound.name == v);
		if (s != null)
		{
			s.source.Play();
			Debug.Log("Playing :" + v);
		}
		else {
			Debug.Log("Sound not found:" + v);
		}
	}

	// Update is called once per frame
	void Update()
    {
        
    }
}
