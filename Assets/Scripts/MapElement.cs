using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
	public class MapElement
	{
		public Color myColor;
		public GameObject myPrefab;
	public bool SpawnPlayer;
	public bool SpawnVictim;
	}
